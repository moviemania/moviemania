<?php 
/*
Template Name: dashboard
*/
 ?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>:: DXN ::</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/main.css" />
		<meta name="mobile-web-app-capable" content="yes">
	 	<meta name="apple-mobile-web-app-capable" content="yes" />
    	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	</head>
	<body>
	<div data-role="page" id="main">
		<div data-role="header" data-position="fixed" class="wow fadeInDown" data-wow-delay="0.2s">
				<a href="" class="ui-btn ui-btn-left wow fadeIn" data-wow-delay='0.8s'><i class="zmdi zmdi-menu"></i></a>
				<h1 class="wow fadeIn" data-wow-delay='0.4s'>Dashboard</h1>
			</div>
			<div role="main" class="ui-content" data-inset="false">
				<div class="nd2-card">

					<div class="card-title has-avatar">
						<img class="card-avatar" src="<?php echo get_stylesheet_directory_uri(); ?>/images/profile_dp.jpg">
						<h3 class="card-primary-title">Power of Lavender</h3>
						<h5 class="card-subtitle">Taken in Provence, France</h5>
					</div>

					<div class="card-media">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/card1.jpg">
					</div>

					<div class="card-action">
						<div class="row between-xs">
							<div class="col-xs-12 align-right">
								<div class="box">
									<a href="#" class="ui-btn ui-btn-inline ui-btn-fab"><i class='zmdi zmdi-favorite'></i></a>
									<a href="#" class="ui-btn ui-btn-inline ui-btn-fab"><i class='zmdi zmdi-bookmark'></i></a>
									<a href="#" class="ui-btn ui-btn-inline ui-btn-fab"><i class='zmdi zmdi-mail-reply zmd-flip-horizontal'></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="nd2-card">

					<div class="card-title has-avatar">
						<img class="card-avatar" src="<?php echo get_stylesheet_directory_uri(); ?>/images/profile_dp.jpg">
						<h3 class="card-primary-title">Power of Lavender</h3>
						<h5 class="card-subtitle">Taken in Provence, France</h5>
					</div>

					<div class="card-media">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/card1.jpg">
					</div>

					<div class="card-action">
						<div class="row between-xs">
							<div class="col-xs-12 align-right">
								<div class="box">
									<a href="#" class="ui-btn ui-btn-inline ui-btn-fab"><i class='zmdi zmdi-favorite'></i></a>
									<a href="#" class="ui-btn ui-btn-inline ui-btn-fab"><i class='zmdi zmdi-bookmark'></i></a>
									<a href="#" class="ui-btn ui-btn-inline ui-btn-fab"><i class='zmdi zmdi-mail-reply zmd-flip-horizontal'></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div data-role="footer" id="" class="bottomsheet" data-position="fixed">
				<div class='row around-xs'>
					<ul class="bottom_tab">
						<li class="bottom_tab">
							<a href="#">
								<i class="icn icn_home_dash"></i>
								<span>Home</span>
							</a>
						</li>
						<li class="bottom_tab">
							<a href="#">
								<i class="icn icn_search_dash"></i>
								<span>Search</span>
							</a>
						</li>
						<li class="bottom_tab upload">
							<a href="#">
								<i class="icn icn_upload_dash"></i>
								<span>New</span>
							</a>
						</li>
						<li class="bottom_tab">
							<a href="#">
								<i class="icn icn_noti_dash"></i>
								<span>Notification</span>
							</a>
						</li>
						<li class="bottom_tab">
							<a href="#">
								<i class="icn icn_setting_dash"></i>
								<span>Settings</span>
							</a>
						</li>
					</ul>
				</div>
			</div>
	</div>	
	 		
	<div id="loader" style="display: block;">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/ajax-loader.gif" alt="">
	</div>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.min.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery-ui.min.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.mobile.min.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/waves.min.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/wow.min.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/nativedroid2.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/nd2settings.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/custom.js"></script>
	</body>
</html>