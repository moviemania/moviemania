<?php 
/*
Template Name: Forgot Password
*/
 ?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>:: DXN ::</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/main.css" />
		<meta name="mobile-web-app-capable" content="yes">
	 	<meta name="apple-mobile-web-app-capable" content="yes" />
    	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	</head>
	<body>
	<div data-role="page" id="main">
		<!-- <nd2-include data-src="includes/welcome.html"></nd2-include> -->
		<div id="signin" class="forgot_password">
			<div data-role="header" data-position="fixed" class="wow transparent-bg" data-wow-delay="0.2s">
				<a href=""<?php echo get_the_permalink(24); ?>" class="ui-btn ui-btn-left wow fadeIn" data-wow-delay='0.8s'><i class="zmdi zmdi-long-arrow-left"></i></a>
				<h1 class="wow fadeIn" data-wow-delay='0.4s'>Forgot Password</h1>
			</div>
			<div id="logo" class="forgot">
				<a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icn_forgot_password.png" alt="movie"></a>
			</div>
			<div class="forgot_content">
				<h2>Forgot Password?</h2>
				<p>We Just need your registerd Email Id or Mobile Number to send you password reset instructions.</p>
			</div>
			<div class="signin_form">
				<div role="main" class="ui-content" data-inset="false">
					 <form class="text-center" style="color: #757575;">
				      <div class="form-group">
				        <label for="name2b">Registered Email or Mobile Number</label>
                    	<input type="text" name="name2" id="name2b" value="" data-clear-btn="true" placeholder="">
				      </div>
				      <button class="btn btn_app btn-block my-4 waves-effect z-depth-0" type="submit">Reset Password</button>
				      <p class="new_member">Already have an account?<a href="<?php echo get_the_permalink(24); ?>">Sign In</a></p>
				    </form>
				</div>
			</div>
		</div>
	</div>			
	<div id="loader" style="display: block;">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/ajax-loader.gif" alt="">
	</div>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.min.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery-ui.min.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.mobile.min.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/waves.min.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/wow.min.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/nativedroid2.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/nd2settings.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/custom.js"></script>
	</body>
</html>