<?php 

// add js files
function my_scripts_method() {
	$theme_url = get_stylesheet_directory_uri();
	wp_enqueue_script( "theme-jquery-min", $theme_url.'/js/jquery.min.js', array(), '', true );
	wp_enqueue_script( "jquery-ui-min", $theme_url.'/js/jquery-ui.min.js', array('theme-jquery-min'), '', true );
	wp_enqueue_script( "jquery-mobile-min", $theme_url.'/js/jquery.mobile.min.js', array('jquery-ui-min'), '', true );
	wp_enqueue_script( "waves-min", $theme_url.'/js/waves.min.js', array('jquery-mobile-min'), '', true );
	wp_enqueue_script( "wow-min", $theme_url.'/js/wow.min.js', array('waves-min'), '', true );
	wp_enqueue_script( "nativedroid2", $theme_url.'/js/nativedroid2.js', array('wow-min'), '', true );
	wp_enqueue_script( "nd2settings", $theme_url.'/js/nd2settings.js', array('nativedroid2'), '', true );
	wp_enqueue_script( "custom", $theme_url.'/js/custom.js', array('nd2settings'), '', true );

	// custom dev
	// Register the script
	wp_register_script( 'custom-dev', $theme_url.'/js/custom-dev.js', array('custom'), '', true  );

	// Localize the script with new data
	$translation_array = array(
		'some_string' => 'prakashrao',
		'a_value' => '10'
	);
	wp_localize_script( 'custom-dev', 'obj', $translation_array );

	// Enqueued script with localized data.
	wp_enqueue_script( 'custom-dev' );


	wp_enqueue_script( "custom", $theme_url.'/js/custom.js', array('nd2settings'), '', true );

}
add_action( 'wp_enqueue_scripts', 'my_scripts_method' );

?>