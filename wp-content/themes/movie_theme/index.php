<!DOCTYPE HTML>
<html>
	<head>
		<title>:: DXN ::</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/main.css" />
		<meta name="mobile-web-app-capable" content="yes">
	 	<meta name="apple-mobile-web-app-capable" content="yes" />
    	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	</head>
	<body>
	<div data-role="page" id="main">
		<!-- <nd2-include data-src="includes/welcome.html"></nd2-include> -->
		<div id="welcome">
			<div id="logo">
				<a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" alt="movie"></a>
			</div>
			<div class="welcome_button">
				<ul>
					<li><a href="<?php echo get_the_permalink(24); ?>">Sign In</a></li>
					<li class="btn_reverse"><a href="<?php echo get_the_permalink(27); ?>">Sign Up</a></li>
				</ul>
			</div>
		</div>
	</div>			
	<div id="loader" style="display: block;">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/ajax-loader.gif" alt="">
	</div>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.min.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery-ui.min.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.mobile.min.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/waves.min.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/wow.min.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/nativedroid2.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/nd2settings.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/custom.js"></script>
	</body>
</html>