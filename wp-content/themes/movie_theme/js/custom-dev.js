// --------------------------------------------------------------
    // Author       : Dxn System
    // Template Name: DXN - Movie App
    // Version      : 1.1 
// --------------------------------------------------------------

/* Login ajax call*/
console.log(obj);
$("#login_form").submit(function(e) {
	var form = $(this);
    var url = form.attr('action');
    $.ajax({
           type: "POST",
           url: url,
           data: form.serialize(), // serializes the form's elements.
           success: function(data)
           {
               console.log(data); // show response from the php script.
           }
         });

    e.preventDefault(); // avoid to execute the actual submit of the form.
});