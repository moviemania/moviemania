<?php 
/*
Template Name: Sign in
*/
get_header();
 ?>

	<div data-role="page" id="main">
		<!-- <nd2-include data-src="includes/welcome.html"></nd2-include> -->
		<div id="signin">
			<div id="logo">
				<a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" alt="movie"></a>
			</div>
			<div class="signin_form">
				<div role="main" class="ui-content" data-inset="false">
					 <form id="ajax_form" class="text-center" style="color: #757575;" action="<?php echo site_url().'/api/user/generate_auth_cookie/'; ?>" method="post">
					      <div class="form-group">
					        <label for="name2b">Username</label>
                        	<input type="text" name="username" id="username" value="" data-clear-btn="true" placeholder="">
					      </div>
					      <div class="form-group">
					        <label for="name2b">Password</label>
                        	<input type="password" name="password" id="password" value="" data-clear-btn="true" placeholder="">
					      </div>
					      <input type="hidden" name="insecure" value="cool">
					      <input type="submit" name="login_submit" value="Sign In" class="btn btn_app btn-block my-4 waves-effect">
					      
					      <p class="forgot_password"><a href="<?php echo get_the_permalink(30); ?>">Forgot Password?</a></p>
					      <p class="new_member">Not a member?<a href="<?php echo get_the_permalink(27); ?>">Sign Up</a></p>
					    </form>
				</div>
			</div>
		</div>
	</div>			
<?php get_footer(); ?>