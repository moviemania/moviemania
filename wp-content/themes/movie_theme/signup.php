<?php 
/*
Template Name: Sign up
*/
 ?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>:: DXN ::</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/main.css" />
		<meta name="mobile-web-app-capable" content="yes">
	 	<meta name="apple-mobile-web-app-capable" content="yes" />
    	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	</head>
	<body>
	<div data-role="page" id="main">
		<!-- <nd2-include data-src="includes/welcome.html"></nd2-include> -->
		<div id="signin">
			<div id="logo">
				<a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" alt="movie"></a>
			</div>
			<div class="signin_form">
				<div role="main" class="ui-content" data-inset="false">
					 <form class="text-center" style="color: #757575;">
				      <div class="form-group">
				        <label for="name2b">Username</label>
                    	<input type="text" name="name2" id="name2b" value="" data-clear-btn="true" placeholder="">
				      </div>
				      <div class="form-group">
				        <label for="name2b">Email</label>
                    	<input type="text" name="name2" id="name2b" value="" data-clear-btn="true" placeholder="">
				      </div>
				      <div class="form-group">
				        <label for="name2b">Mobile</label>
                    	<input type="text" name="name2" id="name2b" value="" data-clear-btn="true" placeholder="">
				      </div>
				      <div class="form-group">
				        <label for="name2b">Password</label>
                    	<input type="password" name="name2" id="name2b" value="" data-clear-btn="true" placeholder="">
				      </div>
				      <button class="btn btn_app btn-block my-4 waves-effect z-depth-0" type="submit">Sign Up</button>
				      <p class="new_member">Already have an account?<a href="<?php echo get_the_permalink(24); ?>">Sign In</a></p>
				    </form>
				</div>
			</div>
		</div>
	</div>			
	<div id="loader" style="display: block;">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/ajax-loader.gif" alt="">
	</div>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.min.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery-ui.min.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.mobile.min.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/waves.min.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/wow.min.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/nativedroid2.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/nd2settings.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/custom.js"></script>
	</body>
</html>