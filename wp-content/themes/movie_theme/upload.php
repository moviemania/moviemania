<?php 
/*
Template Name: upload
*/
 ?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>:: DXN ::</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/main.css" />
		<meta name="mobile-web-app-capable" content="yes">
	 	<meta name="apple-mobile-web-app-capable" content="yes" />
    	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	</head>
	<body>
	<div data-role="page" id="main">
		<div data-role="header" data-position="fixed" class="wow head fadeInDown" data-wow-delay="0.2s">
				<a href="" class="ui-btn ui-btn-left wow fadeIn" data-wow-delay='0.8s'><i class="zmdi zmdi-long-arrow-left"></i></a>
				<h1 class="wow fadeIn" data-wow-delay='0.4s'>Upload your video</h1>
			</div>
			<div role="main" class="ui-content" data-inset="false">
				<div id="upload_form">
					<div class="upload_step">
						<ul id="progressbar">
							<li class="active">Account Setup</li>
							<li>Account Setup</li>
							<li>Account Setup</li>
						</ul>
					</div>
					<div class="upload_content">
						<div class="nd2-card">
							<h3>Upload your video file here</h3>
							<div class="upload_form">
								<div class="upload_row">
									<div class="file_input">
										<input type="file" name="">
									</div>
								</div>
								<div class="upload_row">
									<button type="button" class="btn btn_app" value="Upload" name="">Upload</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div data-role="footer" id="" class="bottomsheet" data-position="fixed">
				<div class='row around-xs'>
					<ul class="bottom_tab">
						<li class="bottom_tab">
							<a href="#">
								<i class="icn icn_home_dash"></i>
								<span>Home</span>
							</a>
						</li>
						<li class="bottom_tab">
							<a href="#">
								<i class="icn icn_search_dash"></i>
								<span>Search</span>
							</a>
						</li>
						<li class="bottom_tab upload">
							<a href="#">
								<i class="icn icn_upload_dash"></i>
								<span>New</span>
							</a>
						</li>
						<li class="bottom_tab">
							<a href="#">
								<i class="icn icn_noti_dash"></i>
								<span>Notification</span>
							</a>
						</li>
						<li class="bottom_tab">
							<a href="#">
								<i class="icn icn_setting_dash"></i>
								<span>Settings</span>
							</a>
						</li>
					</ul>
				</div>
			</div>
	</div>	
	 		
	<div id="loader" style="display: block;">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/ajax-loader.gif" alt="">
	</div>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.min.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery-ui.min.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.mobile.min.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/waves.min.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/wow.min.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/nativedroid2.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/nd2settings.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/custom.js"></script>
	</body>
</html>